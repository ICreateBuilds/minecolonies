package com.minecolonies.lib;

public class Constants
{
    public static final String MOD_ID                = "minecolonies";
    public static final String MOD_NAME              = "MineColonies";
    public static final String VERSION               = "@VERSION@";
    public static final String FINGERPRINT           = "@FINGERPRINT@";
    public static final String CLIENT_PROXY_LOCATION = "com.minecolonies.proxy.ClientProxy";
    public static final String SERVER_PROXY_LOCATION = "com.minecolonies.proxy.ServerProxy";
    public static final String PLAYER_PROPERTY_NAME  = MOD_ID + ".PlayerProperties";
}
